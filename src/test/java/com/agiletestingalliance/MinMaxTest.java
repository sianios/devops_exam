package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class MinMaxTest {

    @Test
    public void testFunctname() {
        MinMax minMax = new MinMax();
        assertEquals(5, minMax.functname(3, 5));
        assertEquals(7, minMax.functname(7, 4));
        assertEquals(10, minMax.functname(10, 10));
    }

    @Test
    public void testBar() {
        MinMax minMax = new MinMax();
        assertEquals("hello", minMax.bar("hello"));
        assertEquals("", minMax.bar(""));
        assertEquals(null, minMax.bar(null));
    }
}
